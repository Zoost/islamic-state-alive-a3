//////////////////////////////////////
//----ALiVE NATOFOR Random Tasks----//
//---By Valixx, Kylania & M4RT14L---//
//---------------v1.4---------------//
//////////////////////////////////////

_missionType = [_this, 0, ""] call BIS_fnc_param;

sleep 120;

_markerArray = ["road","road_1","road_2","road_3","road_4","road_5","road_6","road_7","road_8","road_9","road_10","road_11","road_12","road_13","road_14"];
_rnd 	= floor (random (count(_markerArray)));
_mrkSpawnPos = getMarkerPos (_markerArray select _rnd);

sleep 60;
															   
fn_spawnIedMission = {

	hint "UPDATED SUPPORT OPS";
	//creating the marker 

	_markerSO = createMarker ["mob_ied", _mrkSpawnPos];
	_markerSO setMarkerType "mil_objective";
	_markerSO setMarkerColor "ColorBlue";
	_markerSO setMarkerText "SUPPORT OP";
	_markerSO setMarkerSize [1,1];
	
	_null = [west, "mob_ied", ["Must neutralize the IED.", "Clear IED", "Clear IED"], getMarkerPos "mob_ied", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_ied", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle

	_ied = ["ModuleExplosive_IEDLandSmall_F","ModuleExplosive_IEDLandBig_F"] call BIS_fnc_selectRandom;
	
	explo = createVehicle [_ied,[(getMarkerpos _markerSO select 0) + 3, getMarkerpos _markerSO select 1,0],[], 0, "NONE"];
	
	pole = createVehicle ["FlagSmall_F", getPos explo, [], 0, "NONE"];
	iedblow = pole;
	publicVariable "iedblow";  
	
	grp1S = [getMarkerPos _markerSO, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam")] call BIS_fnc_spawnGroup;
	nul = [grp1S,getMarkerpos _markerSO, 100] call BIS_fnc_taskPatrol;
	
	grp2S = [getMarkerPos _markerSO, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam_AT")] call BIS_fnc_spawnGroup;
	nul = [grp2S,getMarkerpos _markerSO, 100] call BIS_fnc_taskPatrol;

	waitUntil { !alive explo };
	
	_null = ["mob_ied", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerSO;
	{deleteVehicle _x} forEach units grp1S;
	{deleteVehicle _x} forEach units grp2S;
	deleteGroup grp1S;
	deleteGroup grp2S;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_ied"] call LARs_fnc_removeTask;

	sleep 200;
};

fn_spawnRrepairMission = {

	hint "UPDATED SUPPORT OPS";
	//creating the marker 

	_markerSO = createMarker ["mob_rrep", _mrkSpawnPos];
	_markerSO setMarkerType "mil_objective";
	_markerSO setMarkerColor "ColorBlue";
	_markerSO setMarkerText "SUPPORT OP";
	_markerSO setMarkerSize [1,1];
	
	_null = [west, "mob_rrep", ["Take the CRV and repair the MSR.", "Road Repair", "Road Repair"], getMarkerPos "mob_rrep", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_rrep", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerSO, 0, 200, 10, 0, 20, 0] call BIS_fnc_findSafePos;

	_cra = ["CraterLong","CraterLong_small","Dirthump_3_F","Dirthump_2_F","Dirthump_1_F"] call BIS_fnc_selectRandom;
	
	eng = createVehicle ["B_APC_Tracked_01_CRV_F",[(getMarkerpos "respawn_west" select 0) + 10, getMarkerpos "respawn_west" select 1,0],[], 0, "NONE"];
	eng setFuel 1;
	eng allowDammage false;
	_sign = createVehicle ["Sign_Sphere25cm_F",getPos eng, [], 0, "NONE"];
	_sign attachTo [eng,[0,0,+5]];
	
	cra = createVehicle [_cra,[(getMarkerpos _markerSO select 0), getMarkerpos _markerSO select 1,0],[], 0, "NONE"];
	
	grp1S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam")] call BIS_fnc_spawnGroup;
	nul = [grp1S,getMarkerpos _markerSO] call BIS_fnc_taskDefend;
	
	grp2S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam_AT")] call BIS_fnc_spawnGroup;
	nul = [grp2S,getMarkerpos _markerSO] call BIS_fnc_taskDefend;
	

	waitUntil {eng distance getMarkerpos _markerSO < 10};
	
	hint "REPARANDO CARRETERA";
	sleep 60;
	deleteVehicle cra;
	
	_null = ["mob_rrep", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerSO;
	deletevehicle eng;
	deleteVehicle _sign;
	{deleteVehicle _x} forEach units grp1S;
	{deleteVehicle _x} forEach units grp2S;
	deleteGroup grp1S;
	deleteGroup grp2S;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_rrep"] call LARs_fnc_removeTask;

	sleep 200;
};

fn_spawnBuildMission = {

	hint "UPDATED SUPPORT OPS";
	//creating the marker 

	_markerSO = createMarker ["mob_build", _mrkSpawnPos];
	_markerSO setMarkerType "mil_objective";
	_markerSO setMarkerColor "ColorBlue";
	_markerSO setMarkerText "SUPPORT OP";
	_markerSO setMarkerSize [1,1];
	
	_null = [west, "mob_build", ["Take the supply truck and escort him to the AO to deploy the HQ.", "Construir HQ", "Construir HQ"], getMarkerPos "mob_build", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_build", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerSO, 0, 50, 10, 0, 20, 0] call BIS_fnc_findSafePos;
	
	eng = createVehicle ["B_Truck_01_box_F",[(getMarkerpos "respawn_west" select 0) + 10, getMarkerpos "respawn_west" select 1,0],[], 0, "NONE"];
	eng setFuel 1;
	eng allowDammage false;
	_sign = createVehicle ["Sign_Sphere25cm_F",getPos eng, [], 0, "NONE"];
	_sign attachTo [eng,[0,0,+5]];
	
	grp1S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam")] call BIS_fnc_spawnGroup;
	nul = [grp1S,getMarkerpos _markerSO] call BIS_fnc_taskDefend;
	
	grp2S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam_AT")] call BIS_fnc_spawnGroup;
	nul = [grp2S,getMarkerpos _markerSO] call BIS_fnc_taskDefend;

	waitUntil {eng distance getMarkerpos _markerSO < 10};
	
	hint "EN CONSTRUCCION";
	sleep 120;
	post = createVehicle ["Land_Cargo_HQ_V3_F", _newPos,[], 0, "NONE"];
	
	_null = ["mob_build", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerSO;
	deletevehicle eng;
	deleteVehicle _sign;
	deleteVehicle post;
	{deleteVehicle _x} forEach units grp1S;
	{deleteVehicle _x} forEach units grp2S;
	deleteGroup grp1S;
	deleteGroup grp2S;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_build"] call LARs_fnc_removeTask;

	sleep 200;
};

fn_spawnTwrepairMission = {

	hint "UPDATED SUPPORT OPS";
	//creating the marker 

	_markerSO = createMarker ["mob_trepair", _mrkSpawnPos];
	_markerSO setMarkerType "mil_objective";
	_markerSO setMarkerColor "ColorBlue";
	_markerSO setMarkerText "SUPPORT OP";
	_markerSO setMarkerSize [1,1];
	
	_null = [west, "mob_trepair", ["Take the supply truck and escort him to the C.O.P.", "Deliver Supplies", "Deliver Supplies"], getMarkerPos "mob_trepair", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_trepair", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerSO, 0, 50, 10, 0, 20, 0] call BIS_fnc_findSafePos;
	
	eng = createVehicle ["B_Truck_01_box_F",[(getMarkerpos "respawn_west" select 0) + 10, getMarkerpos "respawn_west" select 1,0],[], 0, "NONE"];
	eng setFuel 1;
	eng allowDammage false;
	_sign = createVehicle ["Sign_Sphere25cm_F",getPos eng, [], 0, "NONE"];
	_sign attachTo [eng,[0,0,+5]];
	
	towr = createVehicle ["Land_Cargo_HQ_V3_F",[(getMarkerpos _markerSO select 0) + 10, getMarkerpos _markerSO select 1,0],[], 0, "NONE"];
	
	grp1S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam")] call BIS_fnc_spawnGroup;
	nul = [grp1S,getMarkerpos _markerSO, 150] call BIS_fnc_taskPatrol;
	
	grp2S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam_AT")] call BIS_fnc_spawnGroup;
	nul = [grp2S,getMarkerpos _markerSO, 100] call BIS_fnc_taskPatrol; 

	waitUntil {eng distance getMarkerpos _markerSO < 20};
	
	sleep 60;
	
	_null = ["mob_trepair", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerSO;
	deletevehicle eng;
	deleteVehicle _sign;
	{deleteVehicle _x} forEach units grp1S;
	{deleteVehicle _x} forEach units grp2S;
	deleteGroup grp1S;
	deleteGroup grp2S;
	deleteVehicle towr;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_trepair"] call LARs_fnc_removeTask;

	sleep 200;
};

fn_spawnVerepairMission = {

	hint "UPDATED SUPPORT OPS";
	//creating the marker 

	_markerSO = createMarker ["mob_vrepair", _mrkSpawnPos];
	_markerSO setMarkerType "mil_objective";
	_markerSO setMarkerColor "ColorBlue";
	_markerSO setMarkerText "SUPPORT OP";
	_markerSO setMarkerSize [1,1];
	
	_null = [west, "mob_vrepair", ["They come with the CRV and tow dammaged vehicle to the service the area in Loy Manara AB", "Repair vehicle", "Repair Vehicle"], getMarkerPos "mob_vrepair", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_vrepair", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerSO, 0, 50, 10, 0, 20, 0] call BIS_fnc_findSafePos;

	_dveh = ["B_APC_Tracked_01_rcws_F","B_APC_Tracked_01_AA_F","B_MBT_01_cannon_F"] call BIS_fnc_selectRandom;
	
	eng = createVehicle ["B_APC_Tracked_01_CRV_F",[(getMarkerpos "respawn_west" select 0) + 10, getMarkerpos "respawn_west" select 1,0],[], 0, "NONE"];
	eng setFuel 1;
	eng allowDammage false;
	_sign = createVehicle ["Sign_Sphere25cm_F",getPos eng, [], 0, "NONE"];
	_sign attachTo [eng,[0,0,+5]];
	
	damve = createVehicle [_dveh,[(getMarkerpos _markerSO select 0) + 10, getMarkerpos _markerSO select 1,0],[], 0, "NONE"];
	damve setDammage 0.8;
	damve setFuel 0;
	damve allowDammage false;
	//vehtow = damve;
	//publicVariable "vehtow";
	
	grp1S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam")] call BIS_fnc_spawnGroup;
	nul = [grp1S,getMarkerpos _markerSO, 150] call BIS_fnc_taskPatrol;
	
	grp2S = [_newPos, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam_AT")] call BIS_fnc_spawnGroup;
	nul = [grp2S,getMarkerpos _markerSO, 100] call BIS_fnc_taskPatrol;
	
	waitUntil { eng distance damve < 10};
	sleep 3;
	damve attachTo [eng,[0,-10,0]]; 

	waitUntil { damve distance getMarkerPos "reparea" < 20 };
	
	detach damve;
	sleep 60;
	damve setDammage 0;
	
	_null = ["mob_vrepair", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 60;

	deleteMarker _markerSO;
	{deleteVehicle _x} forEach units grp1S;
	{deleteVehicle _x} forEach units grp2S;
	deleteGroup grp1S;
	deleteGroup grp2S;
	deletevehicle eng;
	deleteVehicle _sign;
	deleteVehicle damve;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_vrepair"] call LARs_fnc_removeTask;

	sleep 240;
};

fn_spawnRescueMission = {

	hint "UPDATED SUPPORT OPS";
	//creating the marker 

	_markerSO = createMarker ["mob_rescue", _mrkSpawnPos];
	_markerSO setMarkerType "mil_objective";
	_markerSO setMarkerColor "ColorBlue";
	_markerSO setMarkerText "SUPPORT OP";
	_markerSO setMarkerSize [1,1];
	
	_null = [west, "mob_rescue", ["Evac the woundeds from the AO and bring to MASH in Loy Manara AB", "MEDEVAC", "MEDEVAC"], getMarkerPos "mob_rescue", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_rescue", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerSO, 0, 50, 10, 0, 20, 0] call BIS_fnc_findSafePos;

	_veh = ["B_MRAP_01_gmg_F","B_MRAP_01_F","B_Truck_01_transport_F"] call BIS_fnc_selectRandom;
	
	_truck = createVehicle [_veh,[(getMarkerpos _markerSO select 0) + 3, getMarkerpos _markerSO select 1,0],[], 0, "NONE"];
	_truck setDammage 0.8;
	_truck allowDamage false;
	_truck setFuel 0;
	
	_grp = createGroup WEST;
	men1 = _grp createUnit ["B_Soldier_F",[(getMarkerpos _markerSO select 0) + 10, getMarkerpos _markerSO select 1,0], [], 0, "NONE"];
	men1 setCaptive true;
	men1 allowDamage false;
	men1 setHit ["hands",1];
	men1 setHit ["head_hit",0.4];
	men1 setHit ["body",0.5];
	men1 playMoveNow "AinjPpneMstpSnonWrflDnon";
	men1 disableAI "MOVE";
	men1 disableAI "ANIM";
	escolta = men1;
	publicVariable "escolta";
	
	men2 = _grp createUnit ["B_Soldier_F",[(getMarkerpos _markerSO select 0) + 15, getMarkerpos _markerSO select 1,0], [], 0, "NONE"];
	men2 setCaptive true;
	men2 allowDamage false;
	men2 setHit ["hands",1];
	men2 setHit ["head_hit",0.4];
	men2 setHit ["body",0.5];
	men2 playMoveNow "AinjPpneMstpSnonWrflDnon";
	men2 disableAI "MOVE";
	men2 disableAI "ANIM";
	escolta = men2;
	publicVariable "escolta";
	
	grp1S = [getMarkerPos _markerSO, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam")] call BIS_fnc_spawnGroup;
	nul = [grp1S,getMarkerpos _markerSO, 100] call BIS_fnc_taskPatrol;
	
	grp2S = [getMarkerPos _markerSO, WEST, (configfile >> "CfgGroups" >> "West" >> "BLU_F" >> "Infantry" >> "BUS_InfTeam_AT")] call BIS_fnc_spawnGroup;
	nul = [grp2S,getMarkerpos _markerSO, 100] call BIS_fnc_taskPatrol;
	
	waitUntil {men1 distance getMarkerPos "mash" < 20;};
	
	[men1,men2] join grpNull;
	
	null = ["mob_rescue", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 10;
	
	deleteMarker _markerSO;
	{deleteVehicle _x} forEach units grp1S;
	{deleteVehicle _x} forEach units grp2S;
	deleteGroup grp1S;
	deleteGroup grp2S;
	deleteVehicle _truck;
	deleteVehicle men1;
	deleteVehicle men2;
	deleteGroup _grp;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_rescue"] call LARs_fnc_removeTask;

	sleep 290;
};

// MAIN LOGIC

_missionDetails = switch (_missionType) do {
	case "ied": {call fn_spawnIedMission;};
	case "roadrepair": {call fn_spawnRrepairMission;};
	case "hqbuild": {call fn_spawnBuildMission;};
	case "towrepair": {call fn_spawnTwrepairMission;};
	case "vehrepair": {call fn_spawnVerepairMission;};
	case "rescue": {call fn_spawnRescueMission;};
};	

nul = [] execVM "scripts\missionSupport.sqf";